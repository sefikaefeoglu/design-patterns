package Factory;

import Factory.Model.Bmw;
import Factory.Service.ArabaFabrika;

public class App 
{
    public static void main( String[] args )
    {
    	
    	ArabaFabrika fabrika = new Bmw(150);
    	
    	System.out.println(fabrika.getArabaListesi().get(0).getBeygirGucu());
        
    }
}
