package Factory.Model;

import Factory.Service.ArabaFabrika;

public class Bmw extends ArabaFabrika {


	public Bmw(int beygirGucu) {
		super(beygirGucu);
	}

	@Override
	public void createAuto(int beygirGucu) {
		getArabaListesi().add(new Z4(beygirGucu));
		
	}

}
