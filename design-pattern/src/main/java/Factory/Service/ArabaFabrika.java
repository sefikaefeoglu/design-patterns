package Factory.Service;

import java.util.ArrayList;

public abstract class ArabaFabrika {
private ArrayList<Araba> arabaListesi = new ArrayList<Araba>();

public ArabaFabrika(int beygirGucu){
	this.createAuto(beygirGucu);
	
};
public abstract void createAuto(int beygirGucu);
public ArrayList<Araba> getArabaListesi() {
	return arabaListesi;
}
public void setArabaListesi(ArrayList<Araba> arabaListesi) {
	this.arabaListesi = arabaListesi;
}

}
